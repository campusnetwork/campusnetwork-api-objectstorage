require('dotenv').config();
const AWS = require('aws-sdk');

const S3_ENDPOINT = process.env.AWS_ENDPOINT || "";
const S3_KEY = AWS.config.credentials.accessKeyId || "";
const S3_SECRET = AWS.config.credentials.secretAccessKey|| "";
const S3_REGION = process.env.AWS_REGION || "";
const S3_BUCKET = process.env.AWS_BUCKET || "";

const clientAwsS3 = new AWS.S3({
    endpoint: S3_ENDPOINT,
    accessKeyId: S3_KEY,
    secretAccessKey: S3_SECRET,
    region: S3_REGION
})


// ===== FILES =====

const getAllFilesS3 = (bucketName) => {
    return new Promise((resolve, reject) => {
        clientAwsS3.listObjectsV2({ Bucket: bucketName, MaxKeys: 5 }, function(err, data) {
            if (err) {
                reject({
                    message: "Error when execute call",
                    error: err.message
                });
            } else if (!data.Contents) {
                reject({
                    message: "No data found"
                });
            } else {
                let objects = [];
                data.Contents.forEach(element => {
                    objects.push({
                        fileName: element.Key,
                        lastModified: element.LastModified
                    });
                });
                resolve({
                    message: `Files from ${bucketName}`,
                    data: objects
                });
            }
        });
    })
}

const getFileS3 = (bucketName, fileKey) => {
    return new Promise((resolve, reject) => {
        clientAwsS3.getObject({Bucket: bucketName, Key: fileKey}, (err, data) => {
            if (err) {
                reject({
                    message: 'Error when execute call for getFileS3',
                    error: err.message
                })
            } else if (!data.Body) {
                reject({
                    message: 'No data found'
                });
            } else {
                resolve({
                    message: `File from ${bucketName}`,
                    data: data.Body
                });
            }
        });
    });
}

const getFileInFolderS3 = (bucketName, folderName, fileKey) => {
    return new Promise((resolve, reject) => {
        clientAwsS3.getObject({Bucket: bucketName, Key: folderName + '/' + fileKey}, (err, data) => {
            if (err) {
                reject({
                    message: 'Error when execute call for getFileS3',
                    error: err.message
                })
            } else if (!data.Body) {
                reject({
                    message: 'No data found'
                });
            } else {
                resolve({
                    message: `File from ${bucketName}`,
                    data: data.Body
                });
            }
        })
    })
}

const uploadFileS3 = ( file, fileName, bucketName = 'bucketcampusnetwork' ) => {
    clientAwsS3.upload({
        Bucket: bucketName,
        Key: fileName,
        Body: file,
        ACL: 'public-read',
        partSize: 5 * 1024,
        queueSize: 10
    })
    .promise()
    .then(data => data.Location)
    .catch(err => err)
}


// ===== BUCKETS =====

const getAllBucketsS3 = () => {
    return new Promise((resolve, reject) => {
        clientAwsS3.listBuckets(function(err, data) {
            if (err) {
                reject({
                    message: 'Error when execute call for getFileS3',
                    error: err.message
                })
            }
            else { 
                resolve({
                    message: `List of buckets`,
                    data: data
                });
            }
        })
    })
}

const createBucketS3 = (bucketName) => {
    return new Promise((resolve, reject) => {
        clientAwsS3.createBucket({ Bucket: bucketName, ACL: 'public-read' }, (err, data) => {
            if (err) {
                reject({
                    message: 'Error when execute call for createBucketS3',
                    error: err.message
                })
            }
            else {
                resolve({
                    message: 'Bucket created',
                    data: data
                })
            }
        })
    })
}


// ===== FOLDERS =====

const getAllFoldersS3 = (bucketName) => {
    return new Promise((resolve,reject) => {
        clientAwsS3.listObjectsV2({ Bucket: bucketName, MaxKeys: 5 }, (err, data) => {
            if (err) {
                reject({
                    message: 'Error when execute call for getAllFoldersS3',
                    error: err.message
                })
            } else if(!data.Contents) {
                reject({
                    message: 'No folders found'
                })
            } else {
                let objects = [];
                let regex = new RegExp('(\.)\w+');
                data.Contents.forEach(element => {
                    if (!regex.exec(element.Key)) {
                        objects.push({
                            fileName: element.Key,
                            lastModified: element.LastModified
                        });
                    }
                });
                resolve({
                    message: `Files from ${bucketName}`,
                    data: objects
                });
            }
        })
    })
}

exports.uploadFileS3 = uploadFileS3;
exports.createBucketS3 = createBucketS3;
exports.getFileS3 = getFileS3;
exports.getAllFilesS3 = getAllFilesS3;
exports.getAllBucketsS3 = getAllBucketsS3;
exports.getFileInFolderS3 = getFileInFolderS3;
exports.getAllFoldersS3 = getAllFoldersS3;
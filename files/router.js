const express = require('express');
const app = express();
const Busboy = require('busboy');
const {uploadFileS3, getFileS3, getFileInFolderS3, getAllFilesS3} = require('../aws-actions');

const InitializeRoute = (app, keycloak)=>{
    app.get('/files/:bucket', keycloak.protect(), (req, res) => {
        getAllFilesS3(req.params.bucket)
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            });
        ;
    })

    app.get('/files/:bucket/get/:key', keycloak.protect(), (req, res) => {
        // if() VERIFIER le param renseignee
        getFileS3(req.params.bucket, req.params.key)
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            });
        ;
    })

    app.get('/files/:bucket/get/:folder/:key', (req,res) => {
        getFileInFolderS3(req.params.bucket, req.params.folder, req.params.key)
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            });
    })

    app.get('/files/:bucket/add/', keycloak.protect(), (req, res) => {
        res.header('Content-Type', 'text/html');
        res.write(`<form action="/files/${req.params.bucket}/add" method="post" enctype="multipart/form-data"><p>Enregistrer une image : </p><input type="file" name="fileUpload" style="display: inline-block;"/><button type="submit">ENVOYER</button></form>`);
        res.end();
    })

    app.post('/files/:bucket/add/', keycloak.protect(), (req, res) => {
        var busboy = new Busboy({ headers: req.headers });
        let fileNameRedirection = '';

        busboy.on('file', function(fieldname, file, filename, encoding, mimetype, bucketname) {
            // console.log('FieldName : ' + fieldname + ' | File : ' + file + ' | FileName : ' + filename + ' | Encoding : ' + encoding + ' | MimeType : ' + mimetype);
            fileNameRedirection = filename;
            uploadFileS3(file, filename, req.params.bucket);

            file.on('data', function(data) {
                // console.log('FieldName : ' + fieldname + ' | File : ' + file + ' | FileName : ' + filename + ' | Encoding : ' + encoding + ' | MimeType : ' + mimetype);
            });

            file.on('end', function() {
                // console.log('FieldName : ' + fieldname + ' | File : ' + file + ' | FileName : ' + filename + ' | Encoding : ' + encoding + ' | MimeType : ' + mimetype);
            });
        });

        busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
            // console.log('FieldName : ' + fieldname + ' | File : ' + file + ' | FileName : ' + filename + ' | Encoding : ' + encoding + ' | MimeType : ' + mimetype);
        });

        busboy.on('finish', function(filename) {
            console.log('Done parsing form!');
            // res.writeHead(303, { Connection: 'close', Location: `/files/${req.params.bucket}/get/${fileNameRedirection}` });
            res.writeHead(303, { Connection: 'close', Location: `/files/${req.params.bucket}` });
            res.end();
        });

        req.pipe(busboy);
    })
}

module.exports = InitializeRoute;
const express = require('express');
const app = express();
const {getAllFoldersS3} = require('../aws-actions');

const InitializeRoute = (app, keycloak)=>{
    app.get('/folders/:bucket', keycloak.protect(), (req, res) => {
        getAllFoldersS3(req.params.bucket)
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            });
        ;
    })
}

module.exports = InitializeRoute;
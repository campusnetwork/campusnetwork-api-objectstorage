# CampusNetwork API ObjectStorage

API permettant d'effectuer des actions CRUD sur le serveur Scaleway Object Storage.

## Pour démarrer

Ces instructions permettent de créer un copie du projet et de le lancer en local sur votre machine pour les développements et les tests.

### Prérequis

Vous devez disposer de :
* Une connexion active vers un serveur de stockage S3
* NodeJS installé sur votre ordinateur

### Installing

* Cloner le repository sur votre machine
* Accéder à la source du dossier depuis un terminal, puis installer les dépendences

```
git clone https://gitlab.com/campusnetwork/campusnetwork-api-objectstorage.git
cd campusnetwork-api-objectstorage
npm install
```

* Ajouter les variables d'environnement et les crédentials AWS/Scaleway.. permettant la connexion à votre serveur S3/ObjectStorage.. (si ces fichier n'existe pas, les créer)

  * Variables d'environnement (fichier '.env' situé à la racine du projet)
    * Region
    * Endpoint


  * Crédentials (fichier '.aws/credentials' situé généralement dans le répertoire 'user' de votre machine)
    * Key
    * Secret

* Lancer le serveur web

```
node server.js
```

#### Exemple d'appel à une route

* Appel de la route permettant de récupérer une photo présente dans un bucket du service S3

  * Ouvrez votre navigateur et entrez l'URL suivante : 'localhost:3002/files/:nomBucket/get/:nomfichier' (remplacer ':nomBucket' par un bucket et ':nomFichier présent sur votre serveur S3)

Une fenêtre permettant de télécharger l'image doit apparaitre.

### Autres

* A chaque modification, penser à recharger le serveur (Ctrl + C ensuite commande de lancement, voir ci-dessus)


## Construit avec

* [NodeJs](https://nodejs.org/en/) - Framework Web utilisé
* [npm](https://www.npmjs.com/) - Gestionnaire de dépendences

## Auteurs

* **Grégory Doucet** - *Initial work* - [GrégoryDoucet](https://gitlab.com/doucetgregory)
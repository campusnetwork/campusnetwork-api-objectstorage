const express = require('express');
const app = express();
const {createBucketS3, getAllBucketsS3} = require('../aws-actions');

const InitializeRoute = (app, keycloak)=>{
    app.get('/', keycloak.protect(), (req, res) => {
        getAllBucketsS3()
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            });
        ;
    })

    app.get('/add', keycloak.protect(), (req, res) => {
        res.header('Content-Type', 'text/html');
        res.write('<form action="/buckets/add" method="post"><p>Créer un bucket : </p><input type="text" name="bucketname" placeholder="Bucket Name" style="display: inline-block;" /><button type="submit">ENVOYER</button></form>');
        res.end();
    })

    app.post('/add', keycloak.protect(), async (req, res) => {
        // A FARE : recup + verification du nom du buket
        createBucketS3('eventimage')
            .then(result => {
                res.send(result.data);
            })
            .catch(err => {
                res.status(500).send(err);
            })
    })
}

module.exports = InitializeRoute;